//
//  UIFont+Extras.m
//  BarX
//
//  Created by Roman Khan on 27/02/16.
//  Copyright © 2017 Zappe. All rights reserved.
//

#import "UIFont+Extras.h"

@implementation UIFont (Extras)

+ (UIFont *)setFontWithType:(NSString *)type withSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:type size:fontSize];
}

@end
