//
//  FlightViewController.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 7/7/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlightViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *hotelbtn;
@property (weak, nonatomic) UIImage *headerImage;
- (IBAction)hotelbtn:(id)sender;

@end
