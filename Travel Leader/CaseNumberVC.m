//
//  CaseNumberVC.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 17/07/20.
//  Copyright © 2020 Gurpreet Singh. All rights reserved.
//

#import "CaseNumberVC.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "ValidationAndJsonVC.h"
#import <QuartzCore/QuartzCore.h>

@interface CaseNumberVC ()
{
    NSDictionary *usaDictionary;
    NSDictionary *GBDictionary;
    BOOL statusofUSCell;
    NSDictionary *INDictionary;
    UIScrollView *bigScrollView;
    
    NSArray *usaStateData;
}
typedef void(^myCompletion)(BOOL);

@end

@implementation CaseNumberVC

- (void)viewDidLoad {
    [super viewDidLoad];
    statusofUSCell = false;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor orangeColor]];

    [self setupLeftMenuButton];
    [self permanentCodeOnView];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
          
          if (networkStatus == NotReachable) {
              [ValidationAndJsonVC displayAlert:nil HeaderMsg:@"No internet connection found!"];

          }
          else {
              [self apiCallingFunction];
          }
    
   
}


-(void)apiCallingFunction {
    
    [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
       [self apiCallForCountry:@"US" :@"" :^(BOOL finished)
       {
           if(finished){
               NSLog(@"success");
               [self apiCallForCountry:@"" :@"" : ^(BOOL finished)
                {
                    if(finished){
                        NSLog(@"Finish");
                        
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                           [self drawScrollviewDataForAllCountry];
                                          [SVProgressHUD dismiss];
                                    });
                        
                    }
                }];
           }
       }];
       
    
}
-(void)setupLeftMenuButton
{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"COVID-19 CASE NUMBERS"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}

-(void)backAction{
    
  [self.navigationController popViewControllerAnimated:NO];
}

-(void)permanentCodeOnView {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
          CGFloat screenWidth = screenRect.size.width;
          CGFloat screenHeight = screenRect.size.height;
       
    UILabel * headingforSource = [[UILabel alloc]initWithFrame:CGRectMake(15, 100, 300, 25)];
    headingforSource.text = @"Data is sourced from Johns Hopkins CSSE";
    headingforSource.font = [UIFont fontWithName:@"Lato-Bold" size:14.0f];
    headingforSource.textColor = [UIColor blackColor];
    [self.view addSubview:headingforSource];


    UILabel * headingocationlbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 135, 150, 30)];
    headingocationlbl.text = @"LOCATIONS";
          headingocationlbl.font = [UIFont fontWithName:@"Lato-Bold" size:15.0f];
          headingocationlbl.textColor = [UIColor blackColor];
          [self.view addSubview:headingocationlbl];
          
          UILabel * totalcaseslbl = [[UILabel alloc]initWithFrame:CGRectMake(165, 135, 200, 30)];
          totalcaseslbl.adjustsFontSizeToFitWidth = YES;
          totalcaseslbl.font = [UIFont fontWithName:@"Lato-Bold" size:15];

          NSString *totalStr = @"CASES / DEATHS / RECOVERED";
          
          NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:totalStr];
          [text addAttribute: NSForegroundColorAttributeName value: [UIColor grayColor] range: NSMakeRange(0, 5)];
          [text addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(8, 7)];
          [text addAttribute: NSForegroundColorAttributeName value: [UIColor greenColor] range: NSMakeRange(17, 9)];
          [totalcaseslbl setAttributedText: text];
          [self.view addSubview:totalcaseslbl];
    
     bigScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(10, 167, screenWidth-20, screenHeight-160)];
         [self.view addSubview:bigScrollView];
    
}

#pragma mark - UI draw for different countries


-(void)drawScrollviewDataForAllCountry {
       CGRect screenRect = [[UIScreen mainScreen] bounds];
       CGFloat screenWidth = screenRect.size.width;
       CGFloat screenHeight = screenRect.size.height;
    
    for (UIView * view in bigScrollView.subviews) {
         [view removeFromSuperview];
    }

   int height = 0;

        
       NSData *data = [[usaDictionary valueForKey:@"CountryData"] dataUsingEncoding:NSUTF8StringEncoding];
       id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
       if ([json isKindOfClass:[NSDictionary class]]) {
    
      UILabel * usaHeadinglbl = [[UILabel alloc]initWithFrame:CGRectMake(0, height, bigScrollView.frame.size.width, 50)];
      // usaHeadinglbl.text = completeUSAString;
       usaHeadinglbl.text = @"       USA";
       usaHeadinglbl.backgroundColor = [UIColor orangeColor];
       usaHeadinglbl.font = [UIFont fontWithName:@"Lato-Bold" size:22.0f];
       usaHeadinglbl.textColor = [UIColor whiteColor];
       usaHeadinglbl.layer.cornerRadius = 4;
       usaHeadinglbl.userInteractionEnabled= YES;
        usaHeadinglbl.layer.masksToBounds = true;
       [bigScrollView addSubview:usaHeadinglbl];
           
           
            
            UIImageView *arrowimage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 20, 14, 9)];
            arrowimage.image = [UIImage imageNamed:@"arrow_down.png"];
            [usaHeadinglbl addSubview:arrowimage];
            
            
            
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
               [button addTarget:self
                                 action:@selector(btnTapped:)
                       forControlEvents:UIControlEventTouchUpInside];
            [button setFrame:CGRectMake(0,height, usaHeadinglbl.frame.size.width , 50)];
        [button setBackgroundColor:[UIColor clearColor]];
            button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:@"States Data  "];
               NSDictionary *attributes0 = @{
                    NSForegroundColorAttributeName:[UIColor whiteColor],
                    NSFontAttributeName:[UIFont boldSystemFontOfSize:18.0f],
                    NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]
               };
               
                [commentString setAttributes:attributes0 range:NSMakeRange(0, [commentString length]-2)];
               [button setAttributedTitle:commentString forState:UIControlStateNormal];
            [usaHeadinglbl addSubview:button];
            
            UILabel * grayBGFornumber = [[UILabel alloc]initWithFrame:CGRectMake(0, height+50, bigScrollView.frame.size.width, 40)];
               grayBGFornumber.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
               [bigScrollView addSubview:grayBGFornumber];
               
               UILabel * namelbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, 150, 30)];
               namelbl.text =@"ACROSS USA";
               namelbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
               namelbl.textColor = [UIColor blackColor];
               [grayBGFornumber addSubview:namelbl];
            
            UILabel * TotalCaselbl = [[UILabel alloc]initWithFrame:CGRectMake(155, 5, 70, 30)];
              TotalCaselbl.adjustsFontSizeToFitWidth = YES;
              TotalCaselbl.text = [NSString stringWithFormat:@"%d",[[json valueForKey:@"TotalConfirmed"]intValue]];
              TotalCaselbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
              TotalCaselbl.textColor = [UIColor grayColor];
              [grayBGFornumber addSubview:TotalCaselbl];
              
              UILabel * Totaldeathlbl = [[UILabel alloc]initWithFrame:CGRectMake(225, 5, 60, 30)];
               Totaldeathlbl.adjustsFontSizeToFitWidth = YES;
              Totaldeathlbl.text = [NSString stringWithFormat:@"%d",[[json valueForKey:@"TotalDeaths"]intValue]];
              Totaldeathlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
              Totaldeathlbl.textColor = [UIColor redColor];
              [grayBGFornumber addSubview:Totaldeathlbl];
              
              UILabel * TotalRecoveredlbl = [[UILabel alloc]initWithFrame:CGRectMake(290, 5, 60, 30)];
           TotalRecoveredlbl.adjustsFontSizeToFitWidth = YES;
              TotalRecoveredlbl.text = [NSString stringWithFormat:@"%d",[[json valueForKey:@"TotalRecovered"]intValue]];
              TotalRecoveredlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                     //TotalRecoveredlbl.textAlignment = NSTextAlignmentRight;
              TotalRecoveredlbl.textColor = [UIColor greenColor];
              [grayBGFornumber addSubview:TotalRecoveredlbl];
        
           height = height+90;
            
            if (statusofUSCell == true) {
                NSString *stateArrayStr = [usaDictionary valueForKey:@"StateData"];
                NSData *data1 = [stateArrayStr dataUsingEncoding:NSUTF8StringEncoding];
                      id json1 = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
                NSArray *stateArray = json1;
                
                for (int j =0; j<stateArray.count; j++) {
                    NSDictionary *dataState = [stateArray objectAtIndex:j];
                    grayBGFornumber = [[UILabel alloc]initWithFrame:CGRectMake(0, height, bigScrollView.frame.size.width, 40)];
                    grayBGFornumber.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
                    [bigScrollView addSubview:grayBGFornumber];
                    
                    if([dataState valueForKey:@"state"]){
                    UILabel * namelbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, 150, 30)];
                    namelbl.text =[dataState valueForKey:@"state"];
                    namelbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                    namelbl.textColor = [UIColor blackColor];
                    [grayBGFornumber addSubview:namelbl];
                    
                    }
                    if([dataState valueForKey:@"positive"]){
                    
                    UILabel * TotalCaselbl = [[UILabel alloc]initWithFrame:CGRectMake(155, 5, 70, 30)];
                       TotalCaselbl.text = [NSString stringWithFormat:@"%d",[[dataState valueForKey:@"positive"]intValue]];
                       TotalCaselbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                        TotalCaselbl.adjustsFontSizeToFitWidth = YES;

                       TotalCaselbl.textColor = [UIColor grayColor];
                       [grayBGFornumber addSubview:TotalCaselbl];
                    }
                        
                        if([dataState valueForKey:@"death"]){
                       UILabel * Totaldeathlbl = [[UILabel alloc]initWithFrame:CGRectMake(225, 5, 60, 30)];
                       Totaldeathlbl.text = [NSString stringWithFormat:@"%d",[[dataState valueForKey:@"death"]intValue]];
                       Totaldeathlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                       Totaldeathlbl.textColor = [UIColor redColor];
                        Totaldeathlbl.adjustsFontSizeToFitWidth = YES;

                       [grayBGFornumber addSubview:Totaldeathlbl];
                       
                        }
                           if([dataState valueForKey:@"recovered"]!=[NSNull null]){
                       UILabel * TotalRecoveredlbl = [[UILabel alloc]initWithFrame:CGRectMake(290, 5, 60, 30)];
                       TotalRecoveredlbl.text = [NSString stringWithFormat:@"%d",[[dataState valueForKey:@"recovered"]intValue]];
                       TotalRecoveredlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                        TotalRecoveredlbl.adjustsFontSizeToFitWidth = YES;
                              //TotalRecoveredlbl.textAlignment = NSTextAlignmentRight;
                       TotalRecoveredlbl.textColor = [UIColor greenColor];
                       [grayBGFornumber addSubview:TotalRecoveredlbl];
                           }
                    height = height +40;
                    
                    
                }
            }
        }
          
    height =  height+40;
    
    
        UILabel * usaHeadinglbl = [[UILabel alloc]initWithFrame:CGRectMake(0, height, bigScrollView.frame.size.width, 50)];
         // usaHeadinglbl.text = completeUSAString;
          usaHeadinglbl.text = @"   REST OF THE WORLD";
          usaHeadinglbl.backgroundColor = [UIColor orangeColor];
          usaHeadinglbl.font = [UIFont fontWithName:@"Lato-Bold" size:20.0f];
          usaHeadinglbl.textColor = [UIColor whiteColor];
          usaHeadinglbl.layer.cornerRadius = 4;
          usaHeadinglbl.userInteractionEnabled= YES;
           usaHeadinglbl.layer.masksToBounds = true;
          [bigScrollView addSubview:usaHeadinglbl];
               
         height =  height+50;
                   NSData *data1 = [[GBDictionary valueForKey:@"CountryData"] dataUsingEncoding:NSUTF8StringEncoding];
                   id json1 = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
                   if ([json1 isKindOfClass:[NSArray class]]) {
           
                       NSArray *countryArray = json1;
           
                       for (int j=0; j<countryArray.count; j++) {
                           NSDictionary *dataDic = [countryArray objectAtIndex:j];
                           
                               UILabel * grayBGFornumber  = [[UILabel alloc]initWithFrame:CGRectMake(0, height, bigScrollView.frame.size.width, 40)];
                               grayBGFornumber.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.5];
                               [bigScrollView addSubview:grayBGFornumber];
                           
                               UILabel * namelbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, 135, 30)];
                               namelbl.text =[dataDic valueForKey:@"Country"];
                               namelbl.adjustsFontSizeToFitWidth =YES;
                               namelbl.font = [UIFont fontWithName:@"Lato-Regular" size:14.0f];
                               namelbl.textColor = [UIColor blackColor];
                               [grayBGFornumber addSubview:namelbl];
                           
                           
                           
                               UILabel * TotalCaselbl = [[UILabel alloc]initWithFrame:CGRectMake(155, 5, 70, 30)];
                               TotalCaselbl.text = [NSString stringWithFormat:@"%d",[[dataDic valueForKey:@"TotalConfirmed"]intValue]];
                               TotalCaselbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                               TotalCaselbl.adjustsFontSizeToFitWidth = YES;

                               TotalCaselbl.textColor = [UIColor grayColor];
                               [grayBGFornumber addSubview:TotalCaselbl];
                           
                               UILabel * Totaldeathlbl = [[UILabel alloc]initWithFrame:CGRectMake(225, 5, 60, 30)];
                               Totaldeathlbl.text = [NSString stringWithFormat:@"%d",[[dataDic valueForKey:@"TotalDeaths"]intValue]];
                               Totaldeathlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                               Totaldeathlbl.textColor = [UIColor redColor];
                               Totaldeathlbl.adjustsFontSizeToFitWidth = YES;

                               [grayBGFornumber addSubview:Totaldeathlbl];
                           
                               UILabel * TotalRecoveredlbl = [[UILabel alloc]initWithFrame:CGRectMake(290, 5, 60, 30)];
                               TotalRecoveredlbl.text = [NSString stringWithFormat:@"%d",[[dataDic valueForKey:@"TotalRecovered"]intValue]];
                               TotalRecoveredlbl.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
                                      //TotalRecoveredlbl.textAlignment = NSTextAlignmentRight;
                               TotalRecoveredlbl.textColor = [UIColor greenColor];
                               TotalRecoveredlbl.adjustsFontSizeToFitWidth = YES;

                               [grayBGFornumber addSubview:TotalRecoveredlbl];
                           
                               height =height+40;
                           
                                      }
                                  
                
       }
        
    
   
    
    bigScrollView.contentSize = CGSizeMake(bigScrollView.frame.size.width, height+100);
}





- (IBAction)btnTapped:(id)sender {
  
    if (statusofUSCell==false) {
        statusofUSCell = true;
        [self drawScrollviewDataForAllCountry ];
    }
    else{
        statusofUSCell=false;
        [self drawScrollviewDataForAllCountry ];
    }
    

}





#pragma mark - Api call for different countries


-(void)apiCallForCountry :(NSString *)countryCode :(NSString *)stateCode :(myCompletion) compblock {
    
    
    NSDictionary *parameters = @{
          @"CountryName": @"",
          @"CountryCode": countryCode,
          @"StateCode":stateCode
          
      };

      NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

      NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://testtravelapi.azurewebsites.net/v1/HealthAdvisory/GetPublicAdvisory"]];
      [request setHTTPMethod:@"POST"];
      [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"content-type"];

      NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    

      NSURLSessionUploadTask *dataTask = [session uploadTaskWithRequest: request
              fromData:data completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
           NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
          NSLog(@"%@", json);
         
          
          if (json) {
        
                  
              if ([countryCode isEqualToString:@"US"]) {
                  
                  usaDictionary = json;
                
              }
              else if ([countryCode isEqualToString:@""]) {
                   GBDictionary = [json mutableCopy];
              }
             
                      
              compblock(YES);


                        }
          }];
      [dataTask resume];
    

    
}

@end
