//
//  LeftDrawerCell.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 6/2/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftDrawerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end
