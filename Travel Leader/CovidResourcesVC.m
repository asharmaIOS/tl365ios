//
//  CovidResourcesVC.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 15/07/20.
//  Copyright © 2020 Gurpreet Singh. All rights reserved.
//

#import "CovidResourcesVC.h"
#import "HomeViewController.h"
#import "WebCheckInVC.h"
#import "USAGuidenceVC.h"
#import "CaseNumberVC.h"
#import "CdcGuidanceVC.h"
#import <QuartzCore/QuartzCore.h>





@interface CovidResourcesVC ()
@property (weak, nonatomic) IBOutlet UILabel *cdcGuideLBl;
@property (weak, nonatomic) IBOutlet UILabel *caseNumberlbl;

@property (weak, nonatomic) IBOutlet UILabel *usaGuidelbl;
@property (weak, nonatomic) IBOutlet UIButton *CDCGuideBtn;
@property (weak, nonatomic) IBOutlet UIButton *USAGuideBtn;
@property (weak, nonatomic) IBOutlet UIButton *CaseNumberBtn;
@property (weak, nonatomic) IBOutlet UIButton *EuropeGuideBtn;


@end

@implementation CovidResourcesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
       [self.navigationController.navigationBar setBarTintColor:[UIColor orangeColor]];

    _CDCGuideBtn.layer.cornerRadius = 4;
    _USAGuideBtn.layer.cornerRadius = 4;
    _CaseNumberBtn.layer.cornerRadius = 4;
    //_EuropeGuideBtn.layer.cornerRadius = 2;
    
    [self drawButtonOnScreen];
    [self setupLeftMenuButton];

}
-(void)backAction
{
    
    [self backButton:self];
}


-(void)drawButtonOnScreen{
    
         CGRect screenRect = [[UIScreen mainScreen] bounds];
          CGFloat screenWidth = screenRect.size.width;
          CGFloat screenHeight = screenRect.size.height;
          
    CGFloat buttonWidth = (screenWidth - 45)/2;
    
    _CDCGuideBtn.frame = CGRectMake(15, _CDCGuideBtn.frame.origin.y, buttonWidth, buttonWidth);
       _CaseNumberBtn.frame = CGRectMake(buttonWidth+30, _CaseNumberBtn.frame.origin.y, buttonWidth, buttonWidth);
       _USAGuideBtn.frame = CGRectMake(15, _CDCGuideBtn.frame.origin.y+buttonWidth+20, buttonWidth, buttonWidth);
     
    _cdcGuideLBl.frame = CGRectMake(15, _CDCGuideBtn.frame.origin.y+ (buttonWidth - 30), buttonWidth, 21);
    _caseNumberlbl.frame = CGRectMake(buttonWidth+30, _CaseNumberBtn.frame.origin.y+ (buttonWidth - 55), buttonWidth, 55);
    _usaGuidelbl.frame = CGRectMake(15, _USAGuideBtn.frame.origin.y+ (buttonWidth - 30), buttonWidth, 21);
    
    
    
}

-(void)setupLeftMenuButton
{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"COVID-19 RESOURCES"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}


-(void)backButton:(id)sender {

 HomeViewController* home = nil;
 for(int vv=0; vv<[self.navigationController.viewControllers count]; ++vv) {
     NSObject *vc = [self.navigationController.viewControllers objectAtIndex:vv];
     if([vc isKindOfClass:[HomeViewController class]]) {
         home = (HomeViewController*)vc;
     }
 }
 
 if (home == nil) {
     // home = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
     
     
     home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
     
     [self.navigationController pushViewController:home animated:NO];
     // Do we need to push it into navigation controller?
 }
 
// [self dismissViewControllerAnimated:YES completion:nil] ;
 
 }


-(IBAction)CDCGuidelineBtn:(id)sender {
//    WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
//    myView.webLink = @"https://www.cdc.gov/coronavirus/2019-ncov/index.html";
//
//    [self.navigationController pushViewController:myView animated:true];
    
    CdcGuidanceVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"CdcGuidanceVC"];

      [self.navigationController pushViewController:myView animated:true];
    
}

-(IBAction)USAGuidelineBtn:(id)sender {
    USAGuidenceVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"USAGuidenceVC"];

    [self.navigationController pushViewController:myView animated:true];

}
-(IBAction)CaseNumberBtn:(id)sender {
    CaseNumberVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"CaseNumberVC"];

    [self.navigationController pushViewController:myView animated:true];

}


@end
