//
//  TripCell.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 8/19/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "TripCell.h"

@implementation TripCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
