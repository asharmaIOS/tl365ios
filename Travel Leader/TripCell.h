//
//  TripCell.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 8/19/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *Airlinelbl;
@property (weak, nonatomic) IBOutlet UILabel *Fromtlbl;
@property (weak, nonatomic) IBOutlet UILabel *tolbl;
@property (weak, nonatomic) IBOutlet UILabel *flightlbl;
@property (weak, nonatomic) IBOutlet UILabel *Tolb;
@property (weak, nonatomic) IBOutlet UIImageView *tripimage;
@property (weak, nonatomic) IBOutlet UILabel *conformationlbl;
@property (weak, nonatomic) IBOutlet UILabel *gatelbl;
@property (weak, nonatomic) IBOutlet UIImageView *lowerlineimgeview;
@property (weak, nonatomic) IBOutlet UIImageView *uparlineimageview;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UIButton *webCheckInBtn;


@end
